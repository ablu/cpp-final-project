#include <iostream>
#include <fstream>
#include <tuple>
#include <map>
#include <cstring>
#include <sstream>
#include <vector>
#include <boost/lockfree/queue.hpp>
#include <boost/asio.hpp>

#include "filecontent.h"
#include "match.h"

const int BLOCK = 50;
const int W_MATCH = 2;
const int W_MISMATCH = -1;
const int W_GAP = -1;
const int THRESHOLD = 70;

int score(const int *buffer1, const int *buffer2, unsigned long start1, unsigned long start2) {
    int matrix_row1[BLOCK + 1] = {0};
    int matrix_row2[BLOCK + 1] = {0};

    int *row1 = matrix_row1;
    int *row2 = matrix_row2;

    int score = std::numeric_limits<int>::min();
    for (unsigned long j = 1; j <= BLOCK; j++) {
        for (unsigned long i = 1; i <= BLOCK; ++i) {
            int value1 = buffer1[start1 + (i - 1)];
            int value2 = buffer2[start2 + (j - 1)];
            int w = value1 == value2 ? W_MATCH : W_MISMATCH;
            row2[i] = std::max(
                    0,
                    std::max(
                            row1[i - 1] + w,
                            std::max(
                                    row2[i - 1],
                                    row1[i]
                            ) + W_GAP
                    )
            );
            score = std::max(score, row2[i]);
        }
        int *tmp = row1;
        row1 = row2;
        row2 = tmp;
    }
    return score;
}

int main(int argc, char **argv) {
    if (argc != 5) {
        std::cerr << "usage: <program> <input 1> <input 2> <output>" << std::endl;
        return 1;
    }

    std::istringstream ss(argv[3]);
    size_t threadCount;
    if (!(ss >> threadCount)) {
        std::cerr << "unable to parse thread count";
        return 1;
    }

    const FileContent sox3(argv[1]);
    const FileContent sry(argv[2]);

    std::map<unsigned long, Match> matches;
    std::mutex matches_lock;

    boost::asio::thread_pool pool(threadCount);

    std::ofstream csvOutput;
    csvOutput.open(argv[4]);

    csvOutput << "start in SOX3,start in SRY,score" << std::endl;
    for (unsigned long i = 0; i < sox3.size - BLOCK; ++i) {
        for (unsigned long j = 0; j < sry.size - BLOCK; ++j) {
            boost::asio::post(pool, [i, j, &sox3, &sry, &matches, &matches_lock]() {
                int newScore = score(sox3.buffer, sry.buffer, i, j);
                if (newScore >= THRESHOLD) {
                    std::scoped_lock lock(matches_lock);
                    if (newScore > matches[i].score) {
                        matches[i].i = i;
                        matches[i].j = j;
                        matches[i].score = newScore;
                    }
                }
            });
        }
    }
    pool.join();

    for (auto entry: matches) {
        const Match &match = entry.second;
        csvOutput << match.i << "," << match.j << "," << match.score << std::endl;
    }

    return 0;
}

