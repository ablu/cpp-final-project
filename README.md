    cmake .
    make
    ./cpp-final-project <input 1> <input 2> <thread count> <output>

If boost is too old:

    wget https://dl.bintray.com/boostorg/release/1.66.0/source/boost_1_66_0.tar.gz
    tar -xf boost_1_66_0.tar.gz
    cmake -DBOOST_ROOT=boost_1_66_0/ .
    # otherwise as above