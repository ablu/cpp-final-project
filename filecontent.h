#pragma once

#include <vector>
#include <sstream>
#include <cstring>
#include <map>
#include <tuple>
#include <fstream>
#include <iostream>

struct FileContent {
    explicit FileContent(const char *filename) {
        std::ifstream ifs(filename);

        std::vector<char> content;

        std::__cxx11::string line;
        getline(ifs, line); // skip first line
        while (getline(ifs, line)) {
            for (char c: line) {
                content.push_back(c);
            }
        }

        auto *int_buffer = (int *) malloc(sizeof(int) * content.size());
        for (unsigned long i = 0; i < content.size(); ++i) {
            int_buffer[i] = (int) content[i];
        }
        size = content.size();
        buffer = int_buffer;
    }

    FileContent(const FileContent &) = delete;

    ~FileContent() {
        free(buffer);
    }

    unsigned long size = 0;
    int *buffer = nullptr;
};